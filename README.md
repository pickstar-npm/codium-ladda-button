## Ladda Button

A Vue wrapper component for [Ladda Buttons](https://github.com/hakimel/Ladda) with some extra nice things and better Promise support.

### Installation

Add package via Yarn:

```
$ yarn add git+ssh://git@code.codium.com.au:vue-components/ladda-button.git
```

Import default styles to your global `scss` file for the project:

```scss
@import "codium-ladda-button/dist/codium-ladda-button.css";
```

Overwrite the styles as necessary.

### Usage

Import the component where you need it:

```javascript
import LaddaButton from 'codium-ladda-button'
```

Define it in your components list:

```javascript
components: {
    LaddaButton
}
```

Use it in your templates:

```html
<ladda-button :click="handleClick" class="btn-default">Submit</ladda-button>
```
### Options

#### `click`

Type: `Function`
Required: `True`

On click callback


#### `arguments`

Type: `Array`
Required: `False`

Any values passed into the arguments attribute will be passed back into the on click callback


#### `laddaStyle`

Type: `String`
Required: `False`
Default: `slide-left`

Changes the spinner animation when clicked


#### `laddaSize`

Type 'String'
Required: 'False'

Changes the ladda button size


#### `laddaColour`

Type: `String`
Required: `False`

Changes the spinner colour


#### `delay`

Type: `Number`
Required: `False`
Default: `500`

Sets a delay time to wait until stopping the spinner after the event has finished


### Install Component Globally

You can install the component globally so it does not need to be done for every component you use it. In a main file:

```javascript
import Vue from 'vue'
import LaddaButton from 'codium-ladda-button'

Vue.component('ladda-button', LaddaButton)
```