module.exports = {
    entry: './src/CodiumLaddaButton.vue',
    filename: {
        js: 'codium-ladda-button.js',
        css: 'codium-ladda-button.css'
    },
    sourceMap: false,
    html: false,
    format: 'cjs'
}